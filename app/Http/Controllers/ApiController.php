<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Link;
use App\Api;
use App\Category;

class ApiController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apis = Api::all();
        return view('apis.index', ['apis' => $apis]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('apis.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!(isset($request->slug) && !empty($request->slug))){
            $request->slug = str_slug(trim($request->name), '-');
        }else{
            $request->slug = str_slug(trim($request->slug), '-');
        }

        $this->validate($request, [
            'name' => 'required|max:255|unique:apis',
            'slug' => 'unique:apis'
        ]);


        Api::create(['name' =>  $request->name, 'slug' => $request->slug]);
        return redirect('/apis');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $api = Api::find($id);
        return view('apis.show',['api' => $api, 'api_id' => $id]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $api = Api::find($id);
        return view('apis.edit',['api' => $api, 'api_id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!(isset($request->slug) && !empty($request->slug))){
            $request->slug = str_slug($request->name, '-');
        }

        $this->validate($request, [
            'name' => 'required|max:255|unique:apis,name,'.$id,
            'slug' => 'unique:apis,slug,'.$id
        ]);

        $api = Api::find($id);
        $api->name = $request->name;
        $api->slug = $request->slug;
        $api->save();
        return redirect('/apis');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $api = Api::find($id);
        $api->delete();

        return redirect('/apis');
    }
};
