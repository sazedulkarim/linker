<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category;
use DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($api_id)
    {
        $categories = Category::where('api_id', $api_id)->get();
        return view('categories.index', ['api_id'=> $api_id,'categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($api_id)
    {

        return view('categories.create',['api_id' => $api_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $api_id)
    {
       
       $this->validate($request, [
        'name' => 'required|max:255',
        ]);

       Category::create(['name' =>  $request->name, 'api_id' => $api_id]);
       return redirect('/dashboard/'.$api_id.'/categories/');
    }
   // api_id
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($api_id,$id)
    {
        $cat = Category::find($id);
        return view('categories.edit',['cat' => $cat,'api_id' => $api_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $api_id, $id)
    {
        $cat = Category::find($id);
        $cat->name = $request->name;
        $cat->save();
        return redirect('/dashboard/'.$api_id.'/categories/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($api_id, $id)
    {
        $cat = Category::find($id);
        $cat->delete();
        
        return redirect('/dashboard/'.$api_id.'/categories/');
    }
}
