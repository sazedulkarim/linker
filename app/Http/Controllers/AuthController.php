<?php

namespace App\Http\Controllers;

use Sentinel;
use Illuminate\Http\Request;

use App\Http\Requests;

class AuthController extends Controller
{
    public function login(){
		return view('users.login');
	}

	public function doLogin(Request $request){
		$messages = [
			'email.required' => 'e-mail address plz!',
			'email.email' => 'This email not exists in our system!',
			'email.exists' => 'This email not exists in our system!',
		];
		$this->validate($request, [
			'email' => 'required|email|exists:users,email',
			'password' => 'required',
		],$messages);
		// get the input fields value 
		$credentials = array(
			'email' => $request->email,
			'password' => $request->password
		);

    		// Authenticate the user  remember_me
		if($request->remember_me=='1'){
			$user = Sentinel::authenticateAndRemember($credentials);
		}else{
			$user = Sentinel::authenticate($credentials);
		}

		if ($user) {
			return redirect('/');
		}

		return view('users.login',['message'=>'Provided e-mail and password not matched!!']);

	}

	// logout method action
	public function doLogout(){
		if (Sentinel::check()) {
			Sentinel::logout();
		}
		return redirect('/auth/login');
	}
}
