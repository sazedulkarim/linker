<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Link;
use App\Category;
use DB;

class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($api_id)
    {
        $links = DB::table('links')
        ->leftJoin('categories', 'links.cat_id', '=', 'categories.id')
        ->where('categories.api_id', $api_id)
        ->select('links.id as id', 'links.name as name', 'links.url','categories.name as category')
        ->get();
        
        //return $links;
        return view('links.index', ['links' => $links,'api_id' => $api_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($api_id)
    {
        $cat_options = [''=>'Select One'];
        $cat_options +=  DB::table('categories')->where('api_id',$api_id)->orderBy('id', 'asc')->lists('name','id');
        return view('links.create',['cat_options'=>$cat_options,'api_id' => $api_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request,'api_id' => $api_id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $api_id)
    {
        $this->validate($request, [
        'name' => 'required|max:255',
        'url' => 'required|max:255|url',
        'cat_id' => 'required',
        ]);

       Link::create(['name' =>  $request->name,'url'=> $request->url,'cat_id' => $request->cat_id ]);
       return redirect('/dashboard/'.$api_id.'/links/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($api_id,$id)
    {
        $link = Link::find($id);
        $cat_options = [''=>'Select One'];
        $cat_options +=  DB::table('categories')->where('api_id',$api_id)->orderBy('id', 'asc')->lists('name','id');
        return view('links.edit',['link' => $link,'api_id' => $api_id,'cat_options' => $cat_options]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$api_id, $id)
    {
        $link = Link::find($id);
        $link->name = $request->name;
        $link->url = $request->url;
        $link->cat_id = $request->cat_id;
        $link->save();
        return redirect('/dashboard/'.$api_id.'/links/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($api_id, $id)
    {
        $link = Link::find($id);
        $link->delete();

        return redirect('/dashboard/'.$api_id.'/links/');
    }
}
