<?php
use App\Link;
use App\Api;
use App\Category;


Route::get('/', ['as' => 'home','middleware'=>'auth' ,'uses' => 'ApiController@index']);
// Login controller
Route::get('/auth/login', ['as' => 'auth.login', 'uses' => 'AuthController@login']);
Route::post('/auth/login', ['as' => 'auth.login.submit','uses' => 'AuthController@doLogin']);
Route::get('/auth/logout', ['as' => 'auth.logout','middleware'=>'auth', 'uses' => 'AuthController@doLogout']);

// admin controller
Route::group(['prefix' => 'dashboard/{api_id}','middleware'=>'auth'], function () {
    Route::resource('categories', 'CategoryController');
	Route::resource('links', 'LinkController');
});

Route::resource('apis', 'ApiController');



/* API ROUTES*/
Route::get('jsonapi/{slug}/categories', function ($slug) {
	$api = Api::where('slug',$slug)->first();
	$categories = Category::where('api_id', $api->id)->get();
	return $categories;
});

Route::get('jsonapi/{slug}/categories/{cat_id}', function ($slug, $cat_id) {	
	$links = Link::where('cat_id', $cat_id)->get();
	return $links;
});