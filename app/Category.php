<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

	protected $fillable = ['name','api_id'];

    protected $hidden = ['api_id'];

	public function apis()
    {
        return $this->belongsTo('App\Api', 'api_id');
    }
	
    public function links()
    {
        return $this->hasMany('App\Link', 'cat_id');
    }
}
