<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    protected $fillable = ['name','slug'];
	
    public function categories()
    {
        return $this->hasMany('App\Category', 'api_id');
    }
}
