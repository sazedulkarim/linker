<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
	protected $fillable = ['name','url','cat_id'];
    
    public function category()
    {
        return $this->belongsTo('App\Category', 'cat_id');
    }
}
