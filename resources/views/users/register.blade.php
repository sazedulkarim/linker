@extends('layouts.auth')

@section('content')


    <div class="row auth-regi-form">
        <div class="col-md-6 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"> <strong class="">Registration</strong>

                </div>
                <div class="panel-body">
                    <p class="text-danger">{{ Session::get('message') }}</p>
                    {{ Form::open(array('route' => 'register.submit','class'=>'register-Form')) }}                    
                        <div class="form-group clearfix @if ($errors->has('first_name')) has-error @endif">
                            <label for="first_name" class="col-sm-3 control-label">First Name</label>
                            <div class="col-sm-9">
                                {{ Form::text('first_name', Input::old('first_name'), array('class' => 'form-control', 'id' => 'first_name', 'placeholder' => 'First Name', 'required' => '')) }}
                                @if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif
                            </div>
                        </div>
                        <div class="form-group clearfix @if ($errors->has('last_name')) has-error @endif">
                            <label for="last_name" class="col-sm-3 control-label">Last Name</label>
                            <div class="col-sm-9">
                                {{ Form::text('last_name', Input::old('last_name'), array('class' => 'form-control', 'id' => 'last_name', 'placeholder' => 'Last Name', 'required' => '')) }}
                                @if ($errors->has('last_name')) <p class="help-block">{{ $errors->first('last_name') }}</p> @endif
                            </div>
                        </div>
                        <div class="form-group clearfix @if ($errors->has('email')) has-error @endif">
                            <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                {{ Form::email('email', Input::old('email'), array('class' => 'form-control', 'id' => 'inputEmail3', 'placeholder' => 'Email', 'required' => '')) }}
                                @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                            </div>
                        </div>

                        <div class="form-group clearfix @if ($errors->has('password')) has-error @endif">
                            <label for="inputPassword3" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9">
                                 {{ Form::password('password', array('class' => 'form-control', 'id' => 'inputPassword3', 'placeholder' => 'Password', 'required' => '')) }}
                                @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
                            </div>
                        </div>
                        <div class="form-group clearfix @if ($errors->has('repassword')) has-error @endif">
                            <label for="inputPassword5" class="col-sm-3 control-label">Password Again</label>
                            <div class="col-sm-9">
                                 {{ Form::password('repassword', array('class' => 'form-control', 'id' => 'inputPassword5', 'placeholder' => 'Password Again', 'required' => '')) }}
                                @if ($errors->has('repassword')) <p class="help-block">{{ $errors->first('repassword') }}</p> @endif
                            </div>
                        </div>

                        <div class="form-group last">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-success btn-sm">Register</button>
                                <button type="reset" class="btn btn-default btn-sm">Reset</button>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
                <div class="panel-footer"> Already member ? <a href="{{url('/')}}" class="">Login Here</a>
                </div>
            </div>
        </div>
    </div>    

@stop