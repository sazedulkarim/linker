@extends('layouts.dashboard')

@section('content')
<div class="col-md-8">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Create a user.</h3>
    </div>
    <div class="panel-body">
     <p class="text-danger">{{ Session::get('message') }}</p>
     {{ Form::open(array('url' => 'users')) }}

     <div class="form-group clearfix @if ($errors->has('first_name')) has-error @endif">
        <label for="first_name" class="col-sm-3 control-label">Name</label>
        <div class="col-sm-9">
            {{ Form::text('first_name', Input::old('first_name'), array('class' => 'form-control', 'id' => 'first_name', 'placeholder' => 'Name', 'required' => '')) }}
            @if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif
        </div>
    </div>
    <div class="form-group clearfix @if ($errors->has('email')) has-error @endif">
        <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
        <div class="col-sm-9">
            {{ Form::email('email', Input::old('email'), array('class' => 'form-control', 'id' => 'inputEmail3', 'placeholder' => 'Email', 'required' => '')) }}
            @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
        </div>
    </div>
    <div class="form-group clearfix @if ($errors->has('mobile_no')) has-error @endif">
        <label for="mobile_no" class="col-sm-3 control-label">Mobile No</label>
        <div class="col-sm-9">
            {{ Form::text('mobile_no', Input::old('mobile_no'), array('class' => 'form-control', 'id' => 'mobile_no', 'placeholder' => 'Mobile No', 'required' => '')) }}
            @if ($errors->has('mobile_no')) <p class="help-block">{{ $errors->first('mobile_no') }}</p> @endif
        </div>
    </div>

    <div class="form-group clearfix @if ($errors->has('password')) has-error @endif">
        <label for="inputPassword3" class="col-sm-3 control-label">Password</label>
        <div class="col-sm-9">
           {{ Form::password('password', array('class' => 'form-control', 'id' => 'inputPassword3', 'placeholder' => 'Password', 'required' => '')) }}
           @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
       </div>
   </div>
   <div class="form-group clearfix @if ($errors->has('repassword')) has-error @endif">
    <label for="inputPassword5" class="col-sm-3 control-label">Password Again</label>
    <div class="col-sm-9">
       {{ Form::password('repassword', array('class' => 'form-control', 'id' => 'inputPassword5', 'placeholder' => 'Password Again', 'required' => '')) }}
       @if ($errors->has('repassword')) <p class="help-block">{{ $errors->first('repassword') }}</p> @endif
   </div>
</div>
<div class="form-group clearfix @if ($errors->has('user_role')) has-error @endif">
    <label for="user_role" class="col-sm-3 control-label">User Role</label>
    <div class="col-sm-9">
       {{Form::select('user_role', $role_options, null, ['class' => 'form-control','id'=>'user_role'])}}
       @if ($errors->has('user_role')) <p class="help-block">{{ $errors->first('user_role') }}</p> @endif
   </div>
</div>

<div class="form-group clearfix">
    <div class="col-md-2 col-md-offset-3">
        {{ Form::submit('Create', array('class' => 'btn btn-primary')) }}
    </div>
</div>

{{ Form::close() }}
</div>
</div>    
</div>
@stop