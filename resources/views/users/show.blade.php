@extends('layouts.dashboard')

@section('content')

    <h1>Showing node {{ $node->node_name }}</h1>

    <div class="jumbotron">      
            <strong>Node IP: </strong> {{ $node->node_ip }}<br>            
            <strong>Node Name: </strong> {{ $node->node_name }}<br>            
            <strong>Node Address: </strong> <address>{{ nl2br($node->node_address) }}</address><br>            
            <strong>Added By: </strong> {{ $node->added_by }}<br>            
            <strong>Added Date: </strong> {{ $node->added_date }}<br>    
    </div>

@stop