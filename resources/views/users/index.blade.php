@extends('layouts.dashboard')

@section('content')
<h2 class="sub-header">User List</h2>
<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info alert-dismissible fade in">
    	<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">&times;</span></button>
    	{{ Session::get('message') }}
    </div>
@endif


<a class="btn btn-primary" href="{{ URL::route('users.create') }}">Create a User</a>
  
<hr>

<div>
	@if(count($users)>0)
	<table class="table table-striped table-bordered table-responsive">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Mobile</th>				
				<th>Status</th>				
				<th>Designation</th>				
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
			<tr{{ $user->activated ? ' class="success"' : ' class="danger"' }}>
				<td>{{ $user->first_name.' '.$user->last_name }}</td>
				<td>{{ $user->email }}</td>
				<td>{{ $user->mobile_no }}</td>				
				<td>{{ $user->activated ? '<p class="text-success"><span aria-hidden="true" class="glyphicon glyphicon-ok-circle"></span> Active</p>' : '<p class="text-warning"><span aria-hidden="true" class="glyphicon glyphicon-ban-circle"></span> Inactive</p>' }}</td>				
				<td>{{ $user->name }}</td>				
				<!-- we will also add show, edit, and delete buttons -->
				<td>
					{{ Form::open(array('url' => 'users/'.$user->id , 'class' => 'pull-left del-button', 'onclick'=>'return confirm(\'Are you sure you want to Delete!!\')')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                	{{ Form::close() }}
					<a class="btn btn-small {{ $user->activated ? 'btn-warning' : 'btn-success' }}" href="{{ URL::route('users.show',$user->id) }}" title="{{ $user->activated ? 'Deactivate' : 'Activate' }}">{{ $user->activated ? '<span aria-hidden="true" class="glyphicon glyphicon-ban-circle"></span>' : '<span aria-hidden="true" class="glyphicon glyphicon-check"></span>' }}</a>
					<a class="btn btn-small btn-success" href="{{ URL::route('users.edit' ,$user->id) }}" title="Edit"><span aria-hidden="true" class="glyphicon glyphicon-edit"></span></a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@else
		<p class="text-warning">Sorry, No results founds.</p>
	@endif

	{{ $users->links() }}
	
</div>

@stop