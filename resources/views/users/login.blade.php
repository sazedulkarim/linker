@extends('layouts.auth')

@section('content')


    <div class="row auth-regi-form">
        <div class="col-md-4 col-md-offset-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong class="">Login</strong>
                </div>
                <div class="panel-body">
                    @include('common.errors')

                    {{ Form::open(array('route' => 'auth.login.submit','class'=>'login-Form')) }}
                    <form class="form-horizontal" role="form">
                        <div class="form-group clearfix @if ($errors->has('email')) has-error @endif">
                            <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                {{ Form::email('email',null, array('class' => 'form-control', 'id' => 'inputEmail3', 'placeholder' => 'Email', 'required' => '')) }}
                            </div>
                        </div>
                        <div class="form-group clearfix @if ($errors->has('password')) has-error @endif">
                            <label for="inputPassword3" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9">
                                 {{ Form::password('password', array('class' => 'form-control', 'id' => 'inputPassword3', 'placeholder' => 'Password', 'required' => '')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <div class="checkbox">
                                    <label class="">
                                        <input name="remember_me" value="1" class="" type="checkbox">Remember me</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group last">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-success btn-sm">Sign in</button>
                                <button type="reset" class="btn btn-default btn-sm">Reset</button>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
                <div class="panel-footer"> 
                    
                </div>
            </div>
        </div>
    </div>    

@stop