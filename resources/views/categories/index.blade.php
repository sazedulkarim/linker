@extends('layouts.dashboard')

@section('content')
<h2 class="sub-header">Categories</h2>
<a href="{{action('CategoryController@create', ['api_id' => $api_id])}}" class="btn btn-primary btn-sm" role="button">Create</a>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Name</th>
        <th>Created</th>
        <th>Last Modified</th>        
        <th>Action</th>        
      </tr>
    </thead>
    <tbody>
      @foreach($categories as $category)
      <tr>
        <td>{{$category->name}}</td>
        <td>{{ $category->created_at->format('d-m-Y') }}</td>
        <td>{{ $category->updated_at->format('d-m-Y') }}</td>
        <td>
        <a style="margin-right: 5px;" href="{{action('CategoryController@edit', ['api_id' => $api_id,'categories'=>$category->id])}}" class="pull-left btn btn-success" role="button"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</a>
        <form action="{{action('CategoryController@destroy', ['api_id' => $api_id,'categories'=>$category->id])}}" method="POST" class="pull-left del-button" onclick="return confirm('Are you sure you want to Delete!!')">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}

            <button type="submit" id="delete-cat-{{ $category->id }}" class="btn btn-danger">
                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
            </button>
        </form>           
          
        </td>        
      </tr>   
      @endforeach
    </tbody>
  </table>
</div>
@endsection