@extends('layouts.dashboard')

@section('content')
<h2 class="sub-header">Edit</h2>
 <div class="panel-body">   
      @include('common.errors')

      <!-- New Task Form -->

     <form action="{{action('CategoryController@update', ['api_id' => $api_id,'categories'=>$cat->id])}}" method="POST">
         {{ method_field('PUT') }}
          <div class="form-group">
              <label for="cat-name">Name</label>
              <input type="text" value="{{$cat->name}}" placeholder="name" name="name" id="cat-name" class="form-control" required="required">              
          </div>

          <button type="submit" class="btn btn-default">
            Update
          </button>
     </form>
  </div>
@endsection