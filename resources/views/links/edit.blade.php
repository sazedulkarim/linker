@extends('layouts.dashboard')

@section('content')
<h2 class="sub-header">Edit</h2>
 <div class="panel-body">   
      @include('common.errors')

      <!-- New Task Form -->

     <form action="{{action('LinkController@update', ['api_id' => $api_id,'categories'=>$link->id])}}" method="POST">
         {{ method_field('PUT') }}
          <div class="form-group">
              <label for="cat-name">Link</label>
              <input type="text" value="{{$link->name}}" placeholder="link" name="name" id="cat-name" class="form-control" required="required">
          </div>
          <div class="form-group">
              <label for="link-url">Link</label>
              <input type="text" value="{{$link->url}}" placeholder="link" name="url" id="link-url" class="form-control" required="required">              
          </div>
          <div class="form-group">
            <label for="cat-id">Category</label>
            {{Form::select('cat_id', $cat_options, $link->cat_id, ['class' => 'form-control','id'=>'cat-id','required' => 'required'])}}
          </div>

          <button type="submit" class="btn btn-default">
            Update
          </button>
      {{ Form::close() }}
  </div>
@endsection