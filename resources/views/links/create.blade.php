@extends('layouts.dashboard')

@section('content')
<h2 class="sub-header">Create New</h2>
 <div class="panel-body">   
      @include('common.errors')
          <form action="{{ url('dashboard/'.$api_id.'/links') }}" method="POST">
          {{ csrf_field() }}

          <!-- Task Name -->
          <div class="form-group">
              <label for="link-name">Name</label>
              <input type="text" placeholder="name" name="name" id="link-name" class="form-control" required="required">              
          </div>
          <div class="form-group">
              <label for="link-url">Link</label>
              <input type="text" placeholder="link" name="url" id="link-url" class="form-control" required="required">              
          </div>
          <div class="form-group">
            <label for="cat-id">Category</label>
            {{Form::select('cat_id', $cat_options, null, ['class' => 'form-control','id'=>'cat-id','required' => 'required'])}}
          </div>
          <button type="submit" class="btn btn-default">
            <i class="fa fa-plus"></i> Add
          </button>
       </form>
  </div>
@endsection