@extends('layouts.dashboard')

@section('content')
<h2 class="sub-header">Links</h2>
<a href="{{action('LinkController@create', ['api_id' => $api_id])}}" class="btn btn-primary btn-sm" role="button">Create</a>
<div class="table-responsive">

  <table class="table table-striped">
    <thead>
      <tr>
        <th>Name</th>
        <th>Link</th>
        <th>Category</th>               
        <th>Action</th>        
      </tr>
    </thead>
    <tbody>
      @foreach($links as $link)
      <tr>
        <td>{{$link->name}}</td>
        <td>{{$link->url}}</td>
        <td>{{$link->category}}</td>               
        <td>
        <a style="margin-right: 5px;" href="{{action('LinkController@edit', ['api_id' => $api_id,'id'=> $link->id])}}" class="pull-left btn btn-success" role="button"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</a>
        <form action="{{action('LinkController@destroy', ['api_id' => $api_id,'id'=> $link->id])}}" method="POST" class="pull-left del-button" onclick="return confirm('Are you sure you want to Delete!!')">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}

            <button type="submit" id="delete-cat-{{ $link->id }}" class="btn btn-danger">
                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
            </button>
        </form>
        </td>        
      </tr>   
      @endforeach
    </tbody>
  </table>
</div>
@endsection