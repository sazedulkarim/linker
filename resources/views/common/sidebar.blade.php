<ul class="nav nav-sidebar">
	@if(Request::is('dashboard/*') || (Request::is('apis/*') && !Request::is('apis/create')))
	<li class="{{ Request::segment(3) === 'categories' ? 'active' : null }}">
		<a href="{{action('CategoryController@index', ['api_id' => $api_id])}}">Categories</a>
	</li>
	<li class="{{ Request::segment(3) === 'links' ? 'active' : null }}">
		<a href="{{action('LinkController@index', ['api_id' => $api_id])}}">Links</a>
	</li>
	@else
	<li class="active">
		<a href="{{route('home')}}">API's</a>
	</li>	
	@endif
</ul>
