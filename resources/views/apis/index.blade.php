@extends('layouts.dashboard')

@section('content')
<h2 class="sub-header">API's</h2>
<a href="{{route('apis.create')}}" class="btn btn-primary btn-sm" role="button">Create</a>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Name</th>
        <th>Slug</th>
        <th>Created</th>
        <th>Last Modified</th>        
        <th>Action</th>        
      </tr>
    </thead>
    <tbody>
      @foreach($apis as $api)
      <tr>
        <td>{{$api->name}}</td>
        <td>{{$api->slug}}</td>
        <td>{{ $api->created_at->format('d-m-Y') }}</td>
        <td>{{ $api->updated_at->format('d-m-Y') }}</td>
        <td>
         <a style="margin-right: 5px;" href="{{route('apis.show',$api->id)}}" class="pull-left btn btn-primary" role="button"><span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span> Dashboard</a>
        <a style="margin-right: 5px;" href="{{route('apis.edit',$api->id)}}" class="pull-left btn btn-success" role="button"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</a>
        <form action="{{ url('apis/'.$api->id) }}" method="POST" class="pull-left del-button" onclick="return confirm('Are you sure you want to Delete!!')">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}

            <button type="submit" id="delete-cat-{{ $api->id }}" class="btn btn-danger">
                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
            </button>
        </form>           
          
        </td>        
      </tr>   
      @endforeach
    </tbody>
  </table>
</div>
@endsection