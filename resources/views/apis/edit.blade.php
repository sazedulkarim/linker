@extends('layouts.dashboard')

@section('content')
<h2 class="sub-header">Edit</h2>
 <div class="panel-body">   
      @include('common.errors')

      <!-- New Task Form -->
      
      {{ Form::model($api, array('route' => array('apis.update', $api->id), 'method' => 'PUT')) }}  

          <div class="form-group">
              <label for="api-name">Name</label>
              <input type="text" value="{{$api->name}}" placeholder="name" name="name" id="api-name" class="form-control" required="required">              
          </div>

          <div class="form-group">
              <label for="api-slug">Slug</label>
              <input type="text" value="{{$api->slug}}" placeholder="slug" name="slug" id="api-name" class="form-control">              
          </div>

          <button type="submit" class="btn btn-default">
            Update
          </button>
      {{ Form::close() }}
  </div>
@endsection