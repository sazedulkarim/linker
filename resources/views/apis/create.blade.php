@extends('layouts.dashboard')

@section('content')
<h2 class="sub-header">Create New</h2>
 <div class="panel-body">   
      @include('common.errors')

      <!-- New Task Form -->
      
      <form action="{{ url('apis') }}" method="POST">
          {{ csrf_field() }}

          <!-- Task Name -->
          <div class="form-group">
              <label for="cat-name">Name</label>
              <input type="text" placeholder="name" name="name" id="cat-name" class="form-control" required="required">              
          </div>
          <div class="form-group">
              <label for="cat-slug">Slug</label>
              <input type="text" placeholder="slug" name="slug" id="cat-slug" class="form-control">              
          </div>

          <button type="submit" class="btn btn-default">
            <i class="fa fa-plus"></i> Add
          </button>
      </form>
  </div>
@endsection

@section('script')

@endsection