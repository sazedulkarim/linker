@extends('layouts.dashboard')

@section('content')

<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">{{$api->name}}</h3> 
	</div> 
	<div class="panel-body">
		<ul class="list-group">
		  <li class="list-group-item list-group-item-success">Api Name: {{$api->name}}</li>
		  <li class="list-group-item list-group-item-info">Api url for categories: <code> {{url('jsonapi/'.$api->slug.'/categories')}}</code></li>
		  <li class="list-group-item list-group-item-warning">Api url for all links of a categories: <code> {{url('jsonapi/'.$api->slug.'/categories/{cat_id}')}}</code></li>		  
		</ul>		
	</div> 
</div>
 
@endsection

@section('script')

@endsection